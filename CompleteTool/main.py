import sys
import os
from selenium import webdriver
import path
import time
import re
from bs4 import BeautifulSoup as bs
from bs4 import BeautifulSoup
import pathlib
import joblib
import numpy as np
import json
import gzip
from selenium import webdriver
import path
import time
import signal
import multiprocessing
import sys
import os
import path
import time
import re
from bs4 import BeautifulSoup as bs
import pathlib
import json

# model libs
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
import numpy as np

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


# load predict url model
modelDetectProductUrl = joblib.load('detect_product_url_model.sav')
modelDetectCategoryUrl = joblib.load('detect_category_url_model.sav')
modelDetectCategoryPage = joblib.load('detect_category_page_model.sav')


###########################################################################
def isHaveSpecifictAddToCartButton(html):
    soup = BeautifulSoup(html,'html.parser')
    classList=[]
    firstClass=0
#     print(soup.find_all(['a','button','input']))
    for i in soup.find_all(['a','button','input']):
        text = i.text
        if(text==''):
            text=i.get('value')
            if not(text):
                text=""
#         print(text)
        if(re.search(re.compile('(add to cart|add to bag|add cart|add to basket|buy it|add to|to checkout|buy now)',re.IGNORECASE),text)):
            print("asdasdad",i.text)
            classList.append(i.get('class'))
    specificClass=[]
    numClass=[]

    for i in classList:
        if not(i in specificClass):
            specificClass.append(i)
            numClass.append(0)

    for i in specificClass:
        print(i)
    
    for i in classList:
        for j in range(0,len(specificClass)):
            if(i == specificClass[j]):
                numClass[j]=numClass[j]+1
    if not numClass:
        return 99999
    print("numclass",numClass)
    return min(numClass)


def crawl_by_block_for_detect_category(url,driver):
    script=""
    crawl_result=""
    with open("mySegment.js", "r") as f:
        script = f.read()
    script += '\n'
    script += 'return segment_page_product();\n'
    try:
        try:
            try:
                driver.get(url)
            except:
                pass
                return []
            try:
                crawl_result = driver.execute_script(script)
            except:
                pass
                return []
            time.sleep(4)
        except:
            pass    
            return []
        
    except:
        pass        
        return []
    return crawl_result


def check_is_product_page(url,driver):
    
    isSpecific=-1
    try:
        try:
            driver.get(url)
        except:
            pass
            
        time.sleep(4)
        isSpecific = isHaveSpecifictAddToCartButton(driver.page_source)
    except:
        pass
    print(isSpecific)
    if(isSpecific>=1 and isSpecific<10):
        return 1
    return 0

def check_is_category_page(crawl_result,model):
    numBlock=0
    totalImgBlock=0
    totalPriceBlock=0
    blockUrl=[]

    for i in range(0,len(crawl_result)):
        numBlock=numBlock+1
        try:
            html = re.sub(r'\s+', ' ', crawl_result[i].get_attribute('outerHTML').strip())
            soup = bs(html,"lxml")
            for i in soup.find_all('img'):
                totalImgBlock=totalImgBlock+1
                break

            for i in soup.find_all(text=True):
                if(re.search(re.compile('[$€]|(VND)|(đ)|(auro)|(dolar)',re.IGNORECASE),i)):
                    totalPriceBlock=totalPriceBlock+1
                    break

        except:
            pass
    
#     print([numBlock,totalPriceBlock,totalImgBlock])
    if(totalPriceBlock>10 and totalImgBlock>10):
        return 1
    return 0

def crawl_all_page(url):
    options = webdriver.firefox.options.Options()
    options.headless = False
    driver = webdriver.Firefox(options=options, executable_path="C:\\geckodriver.exe")
    driver.get(url)
    time.sleep(4)
    page_source=(driver.page_source)
    driver.close()
    return page_source


def convert_to_vector(url):
    sub_array=[]
    sub_array.append(len(re.findall("[-+%_&]",url)))
    sub_array.append(len(re.findall("/",url)))
    sub_array.append(len(url))
    sub_array.append(len(re.findall("[0-9]",url)))
    return sub_array
def clean_url(url):
    remove_domain=re.sub("(https|http)://[w.a-z-A-Z0-9]+/", "", url)
    remove_param=re.sub("\?[a-z0-9,<>?:A-Z!@#'$%^&*)(-/_s=+*]+", "", remove_domain)
    return remove_param
def remove_param_url(url):
    removed=re.sub("\?[a-z0-9,<>?:A-Z!@#'$%^&*)(-/_s=+*]+", "", url)
    return removed

def numSlash(url1):
    return len(re.findall("/",url1))

def addDomain(domainName,url):
    is_contain_domain = re.search("(https|http)://[w.a-z-A-Z0-9]+/", url)
    urlProduct=url
    if not(re.search("^/", urlProduct)):
        if not (is_contain_domain):
            urlProduct="/"+urlProduct
    if not (is_contain_domain):
        urlProduct=domainName+urlProduct
    urlProduct=re.sub("//", "/", urlProduct)
    urlProduct=re.sub(":/", "://", urlProduct)
    return urlProduct

def removeSpace(string):
    return re.sub("[\s]", "", string)

def splitAndFindSimilar(url1,url2):
    url1=url1.split("/")
    url2=url2.split("/")
    
    maxLengthElement=0
    nameProduct=""
    for i in url2:
        if(len(i)>maxLengthElement):
            maxLengthElement=len(i)
            nameProduct=i
    
    countSimilar=0
    for i in url1:
        if(i in url2):
            if(i==nameProduct):
                return 0
            countSimilar=countSimilar+1
    if(countSimilar>0):
        return countSimilar
    else:
        return 0
def similarBetweenSlash(url,arrayString=[]):
    url=url.split("/")
    for i in arrayString:
        for j in url:
            if(i==j):
                return True
    return False

def sortUrlByScore(urls,scores):
    for i in range(0,len(scores)):
        minS=i
        for j in range(i,len(scores)):
            if(float(scores[j])>float(scores[minS])):
                minS=j
        tmp=scores[i]
        scores[i]=scores[minS]
        scores[minS]=tmp

        tmp =urls[i]
        urls[i]=urls[minS]
        urls[minS]=tmp
            
    return urls,scores

def isContainProductString(url):
    characterBetweenSlashProduct=["p","pd","it","t","item","itm","view"]
#     re.compile('(add to cart|add to bag|add cart|add to basket|add to|buy it|add)',re.IGNORECASE)
    if(len(re.findall(re.compile("product|colection|pdp",re.IGNORECASE),url))>0):
        return 1
    arrString=url.split("/")
    for i in arrString:
        if(i in characterBetweenSlashProduct):
            return 1
    return 0

def isContainNotProductString(url):
    characterBetweenSlashNotProduct=["contact"]
    if(len(re.findall(re.compile("signup|cart|signin|about|contact|blog|news|account|tel|photo|image|csv|help|pdf|doc|xlsx|docx|support|login",re.IGNORECASE),url))>0):
        return 1
    arrString=url.split("/")
    for i in arrString:
        if(i in characterBetweenSlashNotProduct):
            return 1
    return 0

def writeToJson(ArrOutputData):
    y=json.dumps({"data":ArrOutputData})
    a=json.loads(y)
    with open('output_product_url.json', 'w') as f:
        json.dump(a, f)

################################################################################

def crawlUrlByBlock(domain,url,driver):
    print("ca dach")
    blockUrl1Url=[]
    blockUrlMoreUrl=[]
    script=""
    with open("mySegment.js", "r") as f:
        script = f.read()
    script += '\n'
    script += 'return segment_page_product();\n'
    try:
        try:
            print("ta")
            driver.get(url)
        except:
            print("pong")
            pass
        
        try:
            crawl_result = driver.execute_script(script)
        except:
            pass
            return []
        time.sleep(4)
        for i in range(0,len(crawl_result)):
#             print("a blockkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
            try:
                html = re.sub(r'\s+', ' ', crawl_result[i].get_attribute('outerHTML').strip())
                soup = bs(html,"lxml")
                if(len(soup.find_all('a', href=True))==1):
                    for a in soup.find_all('a', href=True):
                        blockUrl1Url.append(addDomain(domain,a['href']))
                
                if(len(soup.find_all('a', href=True))):
                    for a in soup.find_all('a', href=True):
                        blockUrlMoreUrl.append(addDomain(domain,a['href']))
            except:
                pass
        
                
    except:
        pass
        
        
        print("error in url",url)
    return blockUrl1Url,blockUrlMoreUrl
    
def addHttp(url):
    return "http://www."+url

####################################################################
def predictListUrlProduct(blockUrl):
    
    urls=[]
    scores=[]
    urlProduct=[]
    for burl in blockUrl:
        url = clean_url(burl)
        url_vect = convert_to_vector(url)
        if(isContainProductString(burl)>0):
            urlProduct.append(burl)
        else:
            if(modelDetectProductUrl.predict([url_vect])==1 and np.amax(modelDetectProductUrl.predict_proba([url_vect]))>=0.8):
                if(isContainNotProductString(burl)==0):
                    urls.append(burl)
                    scores.append(np.amax(modelDetectProductUrl.predict_proba([url_vect])))
    sortedUrl,sortedScore=sortUrlByScore(urls,scores)

    for i in range(0,len(sortedUrl)):
        if(sortedUrl[i] not in urlProduct):
            urlProduct.append(sortedUrl[i])
    return urlProduct


def predictListCategoryBlock(blockUrl):
    
    urls=[]
    scores=[]
    urlCategory=[]
    for burl in blockUrl:
        url = clean_url(burl)
        url_vect = convert_to_vector(url)
    
        if(modelDetectCategoryUrl.predict([url_vect])==1 and np.amax(modelDetectCategoryUrl.predict_proba([url_vect]))>=0.8):
            if(isContainNotProductString(burl)==0):
                urls.append(burl)
                scores.append(np.amax(modelDetectCategoryUrl.predict_proba([url_vect])))
    sortedUrl,sortedScore=sortUrlByScore(urls,scores)

    for i in range(0,len(sortedUrl)):
        if(sortedUrl[i] not in urlCategory):
            urlCategory.append(sortedUrl[i])
            
    return urlCategory


def crawlCategoryPage(domain,url,numUrlNeedCrawl,driverCrawlUrlByBlock,driverCheckIsProductPage,driverCheckIsCategoryPage,urlRecheck=[],crawledUrl=[]):
    if(check_is_category_page(crawl_by_block_for_detect_category(url,driverCheckIsCategoryPage),modelDetectCategoryPage)==1):
        
        maxCrawl=20
        numCrawled=0
        try:
            urlInBlock,moreUrlInBlock=crawlUrlByBlock(domain,url,driverCrawlUrlByBlock)
        except:
            return [],[]
            pass
        
        urlProductSorted=predictListUrlProduct(urlInBlock)
        detectedAUrlProduct=False

        for i in urlProductSorted:
            numCrawled=numCrawled+1
            if(numCrawled>maxCrawl):
                print("stop 1 in category")
                return urlRecheck,crawledUrl
            if(numCrawled>6 and detectedAUrlProduct==False):
                print("stop 2")
                return urlRecheck,crawledUrl
            if(len(urlRecheck)>=numUrlNeedCrawl):
                print("stop 4 in category")
                return urlRecheck,crawledUrl
            if(remove_param_url(i) not in crawledUrl):
                if(check_is_product_page(i,driverCheckIsProductPage)==1):
                    detectedAUrlProduct=True
                    urlRecheck.append(i)


                if(len(urlRecheck)>=numUrlNeedCrawl):
                    print("stop 3 in category")
                    return urlRecheck,crawledUrl
                if(remove_param_url(i) not in crawledUrl):
                    crawledUrl.append(remove_param_url(i))
        print("stop 4 in category")

    return urlRecheck,crawledUrl


def getUrlProduct(domain,url,numUrlNeedCrawl,driverCrawlUrlByBlock,driverCheckIsProductPage,driverCheckIsCategoryPage,layer=1,urlRecheck=[],crawledUrl=[]):
    print("paradon")
    urlRecheck=[]
    maxCrawl=17
    numCrawled=0
    numCrawledCheckCategory=0
    maxCrawlCheckCategory=7
    try:
        urlInBlock,moreUrlInBlock=crawlUrlByBlock(domain,url,driverCrawlUrlByBlock)
    except:
        return []
        pass
    urlProductSorted=predictListUrlProduct(urlInBlock)
    urlCategorySorted=predictListCategoryBlock(moreUrlInBlock)
    
    detectedAUrlProduct=False
    for i in urlProductSorted:
        numCrawled=numCrawled+1
        if(numCrawled>maxCrawl):
            print("stop 1")
            return urlRecheck
        if(numCrawled>=7 and detectedAUrlProduct==False):
            print("stop 2")
            return urlRecheck
        if(remove_param_url(i) not in crawledUrl):
            if(check_is_product_page(i,driverCheckIsProductPage)==1):
                detectedAUrlProduct=True
                urlRecheck.append(i)
                crawledUrl.append(remove_param_url(i))
            else:
                if(layer==1):
                    if(check_is_category_page(crawl_by_block_for_detect_category(i,driverCheckIsCategoryPage),modelDetectCategoryPage)==1):
                        if(layer==1):
                            for j in getUrlProduct(domain,i,numUrlNeedCrawl-len(urlRecheck),driverCrawlUrlByBlock,driverCheckIsProductPage,driverCheckIsCategoryPage,2,urlRecheck,crawledUrl):
                                if(j not in urlRecheck):
                                    urlRecheck.append(j)
                            crawledUrl.append(remove_param_url(i))
                        print("category page:",i)
                    else:
                        crawledUrl.append(remove_param_url(i))
            if(len(urlRecheck)>=numUrlNeedCrawl):
                print("stop 3")
                return urlRecheck
        else:
            numCrawled=numCrawled-1

    if(len(urlRecheck)>=numUrlNeedCrawl):
        return urlRecheck
    else:
        for i in urlCategorySorted:
            if(remove_param_url(i) not in crawledUrl):
                if(check_is_product_page(i,driverCheckIsProductPage)==1):
                    urlRecheck.append(i)
                    crawledUrl.append(remove_param_url(i))
                else:
                    tmpUrlProducts,tmpUrlCrawleds=crawlCategoryPage(domain,i,numUrlNeedCrawl-len(urlRecheck),driverCrawlUrlByBlock,driverCheckIsProductPage,driverCheckIsCategoryPage,[],crawledUrl)
                    for tmpUrlProduct in tmpUrlProducts:
                        if(tmpUrlProduct not in urlRecheck):
                            urlRecheck.append(tmpUrlProduct)
                    for tmpUrlCrawled in tmpUrlCrawleds:
                        if(remove_param_url(tmpUrlCrawled) not in crawledUrl):
                            crawledUrl.append(remove_param_url(tmpUrlCrawled))
                    numCrawledCheckCategory=numCrawledCheckCategory+1
            if(len(urlRecheck)>=numUrlNeedCrawl):
                return urlRecheck
            if(numCrawledCheckCategory>maxCrawlCheckCategory):
                return urlRecheck
    return urlRecheck


def threadCrawlUrl(domain):
    print("asdasd")
    url=[]
    print("ala1",url)
    start = time.time()
    timeCheck=0

    try:
        url=getUrlProduct(addHttp(listShopUrl[domain].lower()),addHttp(listShopUrl[domain].lower()),needCrwal,driverCrawlUrlByBlock,driverCheckIsProductPage,driverCheckIsCategoryPage)

    except:
        print("Sadadasd")
        pass

    end = time.time()
    delta = end - start
    product={
        "domain":addHttp(listShopUrl[i].lower()),
        "product":url,
        "ordinal":i,
        "time_execute":delta
    }
    print("ala3",url)
    print("in number page",i)
    print ("took %.2f seconds to process" % delta)
    ArrOutputData.append(product)
    writeToJson(ArrOutputData)




# bar
def bar(a,outVal):
    for i in range(8):
        outVal.append(i)
        print(a)
        time.sleep(1)

###############################################################################
needCrwal=10
ArrOutputData=[]

optionsDriverCrawlUrlByBlock = webdriver.firefox.options.Options()
optionsDriverCrawlUrlByBlock.headless = False
driverCrawlUrlByBlock = webdriver.Firefox(options=optionsDriverCrawlUrlByBlock, executable_path="/usr/local/bin/geckodriver")
driverCrawlUrlByBlock.implicitly_wait(120)
driverCrawlUrlByBlock.set_page_load_timeout(120)
driverCrawlUrlByBlock.set_script_timeout(120)

optionsDriverCheckIsProductPage = webdriver.firefox.options.Options()
optionsDriverCheckIsProductPage.headless = False
driverCheckIsProductPage = webdriver.Firefox(options=optionsDriverCheckIsProductPage, executable_path="/usr/local/bin/geckodriver")

optionsDriverCheckIsCategoryPage = webdriver.firefox.options.Options()
optionsDriverCheckIsCategoryPage.headless = False
driverCheckIsCategoryPage = webdriver.Firefox(options=optionsDriverCheckIsCategoryPage, executable_path="/usr/local/bin/geckodriver")

##################################################################################
listShopUrl=['pottymd.com']
urlProduct = []
try:
    for i in range(0,len(listShopUrl)):
        url=[]
        print("ala1",url)
        start = time.time()
        # do something
        timeCheck=0
        
        url=getUrlProduct(addHttp(listShopUrl[i].lower()),addHttp(listShopUrl[i].lower()),needCrwal,driverCrawlUrlByBlock,driverCheckIsProductPage,driverCheckIsCategoryPage)
        urlProduct = url
        end = time.time()
        delta = end - start
        product={
            "domain":addHttp(listShopUrl[i].lower()),
            "product":url,
            "ordinal":i,
            "time_execute":delta
        }
        print("ala3",url)
        print("in number page",i)
        print ("took %.2f seconds to process" % delta)
        ArrOutputData.append(product)
        writeToJson(ArrOutputData)
except:
    pass
    writeToJson(ArrOutputData)

writeToJson(ArrOutputData)

#############################################################################


# build model
PatternString = np.load('PatternString.npy').tolist()
PatternLabel = np.load('PatternLabel.npy').tolist()

from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier

text_clf = Pipeline([
     ('vect', CountVectorizer(stop_words="english",ngram_range=(1,2))),
     ('clf', SGDClassifier(loss='modified_huber', penalty='l2',
                           alpha=1e-3, random_state=42,
                           max_iter=5, tol=None)),
])

text_clf.fit(PatternString, PatternLabel)
print("")
########################################################
def appendDarkPattern(darkPatterns,dpType,dpString):
    isContaint=0
    for i in darkPatterns:
        if(i["type"]==dpType):
            isContaint=1
            i["instance"].append(dpString)
    if(isContaint==0):
        darkPatterns.append({"type":dpType,"instance":[dpString]})
        
    return darkPatterns
            
    
def detectDarkPattern(driver,url):
    # Crawler
    darkPatterns=[]
    script = ""
    
    with open(os.path.join(sys.path[0], "mySegment.js"), "r") as f:
        script = f.read()
    script += '\n'
    
    driver.get(url)


    time.sleep(5)


    script += 'return segment_page();\n'
    crawl_result = driver.execute_script(script)
   

    for i in range(0,len(crawl_result)):
        html = re.sub(r'\s+', ' ', crawl_result[i].get_attribute('outerHTML').strip())
        soup = bs(html,"lxml")
        for rs in soup.find_all('div'):
            test=[rs.text]
            if(np.amax(text_clf.predict_proba(test))>=0.8):
#                 print(rs.text,np.amax(text_clf.predict_proba(test)),text_clf.predict(test))
                appendDarkPattern(darkPatterns,text_clf.predict(test)[0],rs.text)
    return darkPatterns

def writeToFile(data,num):
    with open("dataDetect/data"+str(num)+".json", 'w') as outfile:
        json.dump({"data":data}, outfile)

####################################################################
optionsDriverDetectDarkPattern = webdriver.firefox.options.Options()
optionsDriverDetectDarkPattern.headless = False
driverDetectDarkPattern = webdriver.Firefox(options=optionsDriverCheckIsProductPage, executable_path="/usr/local/bin/geckodriver")

driverDetectDarkPattern.implicitly_wait(120)
driverDetectDarkPattern.set_page_load_timeout(120)
driverDetectDarkPattern.set_script_timeout(120)

#######################################################################
total=0
mainArr=[]
print(urlProduct)
i=0

resultDarkPatterns = []

for i in range(0,len(urlProduct)):
    darkPatterns = detectDarkPattern(driverDetectDarkPattern,urlProduct[i])
    resultDarkPatterns.append({
        "product_site":urlProduct[i],
        "dark_patterns":darkPatterns
    })
print(resultDarkPatterns)