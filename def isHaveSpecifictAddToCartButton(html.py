def isHaveSpecifictAddToCartButton(html):
    soup = BeautifulSoup(html,'html.parser')
    classList=[]
    firstClass=0
    for i in soup.find_all(['a','button','input','span']):
        if(re.search(re.compile('(add to cart|add to bag|add cart)',re.IGNORECASE),i.text)):
            classList.append(i.get('class'))
    specificClass=[]
    numClass=[]
    for i in classList:
        if not(i in specificClass):
            specificClass.append(i)
            numClass.append(0)

    for i in classList:
        for j in range(0,len(specificClass)):
            if(i == specificClass[j]):
                numClass[j]=numClass[j]+1
    if not numClass:
        return 99999
    return min(numClass)

def crawler(url):
    
    options = webdriver.firefox.options.Options()
    options.headless = False
    driver = webdriver.Firefox(options=options, executable_path=r'/usr/local/bin/geckodriver')
    driver.get(url)
    time.sleep(4)
    isSpecific = isHaveSpecifictAddToCartButton(driver.page_source)
    driver.close()
    if(isSpecific==1):
        return 1
    return 0

def crawl_all_page(url):
    options = webdriver.firefox.options.Options()
    options.headless = False
    driver = webdriver.Firefox(options=options, executable_path=r'/usr/local/bin/geckodriver')
    driver.get(url)
    time.sleep(4)
    page_source=(driver.page_source)
    driver.close()
    return page_source


def convert_to_vector(url):
    sub_array=[]
    sub_array.append(len(re.findall("[-+%_&]",url)))
    sub_array.append(len(re.findall("/",url)))
    sub_array.append(len(url))
    return sub_array
def clean_url(url):
    remove_domain=re.sub("(https|http)://[w.a-zA-Z0-9]+/", "", url)
    remove_param=re.sub("\?[a-z0-9,<>?:A-Z!@#'$%^&*)(-/_s=+*]+", "", remove_domain)
    return remove_param

def numSlash(url1):
    return len(re.findall("/",url1))

def addDomain(domainName,url):
    is_contain_domain = re.search("(https|http)://[w.a-zA-Z0-9]+/", url)
    urlProduct=url
    if not (is_contain_domain):
        urlProduct=domainName+urlProduct
    urlProduct=re.sub("//", "/", urlProduct)
    urlProduct=re.sub(":/", "://", urlProduct)
    return urlProduct

def removeSpace(string):
    return re.sub("[\s]", "", string)

def splitAndFindSimilar(url1,url2):
    url1=url1.split("/")
    url2=url2.split("/")
    
    maxLengthElement=0
    nameProduct=""
    for i in url2:
        if(len(i)>maxLengthElement):
            maxLengthElement=len(i)
            nameProduct=i
    
    countSimilar=0
    for i in url1:
        if(i in url2):
            if(i==nameProduct):
                return 0
            countSimilar=countSimilar+1
    if(countSimilar>0):
        return countSimilar
    else:
        return 0
def similarBetweenSlash(url,arrayString=[]):
    url=url.split("/")
    for i in arrayString:
        for j in url:
            if(i==j):
                return True
    return False


def crawlInProductPage(urlProductDetected,urlProductDetectedNotDomain,crawledUrl,num_product_url,domain_name):
    htmlSource=crawl_all_page(urlProductDetected)
    listFindNumsimilar=[]
    productURLClean=[]
    url=''
    soup = BeautifulSoup(htmlSource,'html')
    numSimilarElement=0
    numpageCrawlInAcceptable=15
    numPageCrawl=0
#     print("in layer",layer)
    for a in soup.find_all('a', href=True):
        url = clean_url(a['href'])
        url_vect = convert_to_vector(url)
        if(model.predict([url_vect])==1 and np.amax(model.predict_proba([url_vect]))>=0.8):
            listFindNumsimilar.append(url)

    for i in range(0,len(listFindNumsimilar)):
        if not (i==urlProductDetectedNotDomain):
            if(splitAndFindSimilar(listFindNumsimilar[i],urlProductDetectedNotDomain)>=1 and listFindNumsimilar[i]!=urlProductDetectedNotDomain):
                numSimilarElement=splitAndFindSimilar(listFindNumsimilar[i],urlProductDetectedNotDomain)
                    
    for a in soup.find_all('a', href=True):
        url = clean_url(a['href'])
        if(numSlash(url)==numSlash(urlProductDetectedNotDomain) and splitAndFindSimilar(urlProductDetectedNotDomain,url)>=numSimilarElement):
            urlProduct=addDomain(domain_name,a['href'])
#             if not (crawler(urlProduct)==0):
            numPageCrawl=numPageCrawl+1
            if not (urlProduct in crawledUrl):
                crawledUrl.append(urlProduct)
                num_product_url=num_product_url+1
                productURLClean.append(urlProduct)
        if(numPageCrawl>numpageCrawlInAcceptable):
            break
        if(num_product_url==10):
            break
    return productURLClean,num_product_url





listStringNeedCrawl=["b","itm"]

def extract_url_product(model,htmlSource,domain_name,layer=1,notUrlProduct=[],num_product_url=0,crawledUrl=[]):
    
    listFindNumsimilar=[]
    productURLClean=[]
    url=''
    detectedProductUrl=False
    numSlashURLProduct=0
    firstProductDetected=""
    soup = BeautifulSoup(htmlSource,'html')
    numSimilarElement=0
    numpageCrawlInLayer2=0
    numpageCrawlInLayer2Acceptable=5
    numpageCrawlInLayer1=0
#     print("in layer",layer)
    for a in soup.find_all('a', href=True):
        url = clean_url(a['href'])
        url_vect = convert_to_vector(url)
        if(model.predict([url_vect])==1 and np.amax(model.predict_proba([url_vect]))>=0.9):
            listFindNumsimilar.append(url)

    for i in range(0,len(listFindNumsimilar)):
        for j in range(0,len(listFindNumsimilar)):
            if not (i==j):
                if(splitAndFindSimilar(listFindNumsimilar[i],listFindNumsimilar[j])>=1 and listFindNumsimilar[i]!=listFindNumsimilar[j]):
                    numSimilarElement=splitAndFindSimilar(listFindNumsimilar[i],listFindNumsimilar[j])
                    
#     print(numSimilarElement)
    for a in soup.find_all('a', href=True):
#         print("run")
        url = clean_url(a['href'])
        if(detectedProductUrl):
            if(numSlash(url)==numSlashURLProduct):
                isSimilar=True
                for i in productURLClean:
                    if not (splitAndFindSimilar(firstProductDetected,url)>=numSimilarElement):
                        isSimilar=False
                        break
                if(isSimilar):
                    urlProduct=addDomain(domain_name,a['href'])
                    if(layer==2):
                        numpageCrawlInLayer2=numpageCrawlInLayer2+1
                        if(numpageCrawlInLayer2>=numpageCrawlInLayer2Acceptable):
                            return productURLClean,num_product_url,notUrlProduct,crawledUrl

                    if(layer==1):
                        numpageCrawlInLayer1=numpageCrawlInLayer1+1
                        print(numpageCrawlInLayer1)
                        if(numpageCrawlInLayer1>20):
                            return productURLClean,num_product_url,notUrlProduct,crawledUrl

                    if not (crawler(urlProduct)==0):
                        if not (urlProduct in crawledUrl):
                            crawledUrl.append(urlProduct)
                            num_product_url=num_product_url+1
                            productURLClean.append(urlProduct)
        
        else:
            url = clean_url(a['href'])
            url_vect = convert_to_vector(url)
            
            notUrlProdcut=False
            if(layer==2): 
                for i in range(0,len(notUrlProduct)):
                    if((numSlash(notUrlProduct[i])==numSlash(url) and splitAndFindSimilar(notUrlProduct[i],url)>=numSimilarElement) and numSimilarElement!=0):
                        notUrlProdcut=True
#                         print("run not product url")
#                         print(url)
                        break
            if not(notUrlProdcut):
                if((model.predict([url_vect])==1 and np.amax(model.predict_proba([url_vect]))>=0.8) or similarBetweenSlash(url,listStringNeedCrawl)):
                    urlProduct=addDomain(domain_name,a['href'])

                    if(removeSpace(urlProduct) not in crawledUrl):
                        if not (crawler(urlProduct)==0):
                            
#                             print("detedted first product url")
                            numSlashURLProduct=len(re.findall("/",url))
                            firstProductDetected=url
                            detectedProductUrl=True
                            tmpUrlWithHeadSlash=url
                            if(layer==2):
                                numpageCrawlInLayer2Acceptable=15
            
                            if(re.search("^[a-z]{1,20}\/",tmpUrlWithHeadSlash)):
                                tmpUrlWithHeadSlash="/"+tmpUrlWithHeadSlash
                    
                            x=tmpUrlWithHeadSlash.split("/")
#                             for i in x:
#                                 if re.search("^[a-z]{1,20}$",i):
#                                     match=re.search("^[a-z]{1,20}$",i)
#                                     listStringNeedCrawl.append(match.group(0))

                            crawledUrl.append(urlProduct)
                            print(urlProduct)
                            num_product_url=num_product_url+1
                            productURLClean.append(urlProduct)
                            
#                             htmlSource=crawl_all_page(addDomain(domain_name,a['href']))
#                             layer=2
#                             listProductUrlTmp,numProductUrlTmp,notUrlProductTmp,tmpCrawledUrl=extract_url_product(model,htmlSource,domain_name,layer,notUrlProduct,firstProductDetected,numSlashURLProduct,detectedProductUrl,num_product_url,crawledUrl)
# #                             print("crawled in layer",layer)
#                             for i in listProductUrlTmp:
#                                 productURLClean.append(i)
#                             for i in tmpCrawledUrl:
#                                 if(removeSpace(i) not in crawledUrl):
#                                     crawledUrl.append(removeSpace(i))
#                             num_product_url=num_product_url+numProductUrlTmp
#                             layer=1
                        else:
#                             print("ala con da")
                            crawledUrl.append(addDomain(domain_name,a['href']))
                            if(layer==1):
                                numpageCrawlInLayer1=numpageCrawlInLayer1+1
                                print(numpageCrawlInLayer1)
                            notUrlProduct.append(url)
                            if(layer==1):
                                if(numpageCrawlInLayer1>20):
                                    return productURLClean,num_product_url,notUrlProduct,crawledUrl
                        
                            if(layer==2):
                                numpageCrawlInLayer2=numpageCrawlInLayer2+1
                                if(numpageCrawlInLayer2>=numpageCrawlInLayer2Acceptable):
                                    return productURLClean,num_product_url,notUrlProduct,crawledUrl
                            if(layer==1):
                                htmlSource=crawl_all_page(addDomain(domain_name,a['href']))
                                layer=2
                                listProductUrlTmp,numProductUrlTmp,notUrlProductTmp,tmpCrawledUrl=extract_url_product(model,htmlSource,domain_name,layer,notUrlProduct,num_product_url,crawledUrl)
#                                 print("crawled in layer",layer)
                                for i in listProductUrlTmp:
                                    productURLClean.append(i)
                                for i in tmpCrawledUrl:
                                    if(removeSpace(i) not in crawledUrl):
                                        crawledUrl.append(removeSpace(i))
                                num_product_url=num_product_url+numProductUrlTmp
                                layer=1
#                 else:
#                     print("not predict",a['href'])

        if(num_product_url>=10):
            break
        
    return productURLClean,num_product_url,notUrlProduct,crawledUrl