# crawler lib
import sys
import os
from selenium import webdriver
import path
import time
import re
from bs4 import BeautifulSoup as bs
import pathlib

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


# model libs
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
# from imblearn.pipeline import make_pipeline
# from imblearn.combine import SMOTEENN
from sklearn.linear_model import SGDClassifier
import numpy as np


# build model
PatternString = np.load(os.path.join(__location__, 'PatternString.npy')).tolist()
PatternLabel = np.load(os.path.join(__location__, 'PatternLabel.npy')).tolist()

from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier

text_clf = Pipeline([
     ('vect', CountVectorizer(stop_words="english",ngram_range=(1,2))),
     ('tfidf', TfidfTransformer()),
     ('clf', SGDClassifier(loss='modified_huber', penalty='l2',
                           alpha=1e-3, random_state=42,
                           max_iter=5, tol=None)),
])


text_clf.fit(PatternString, PatternLabel)



# load segment page script
script = ''


with open(os.path.join(sys.path[0], "mySegment.js"), "r") as f:
    script = f.read()
script += '\n'


def detectDarkPattern(url):
    # Crawler
    url="https://www.amazon.com/FolkArt-34879-Furniture-Assorted-Sheepskin/dp/B01E9GTM3Q/ref=sr_1_18?dchild=1&keywords=paint&qid=1586608382&sr=8-18"
    script=""

    options = webdriver.firefox.options.Options()
    options.headless = True
    driver = webdriver.Firefox(options=options, executable_path=r'/usr/local/bin/geckodriver')

    driver.get(url)


    time.sleep(8)


    script += 'return segment_page();\n'
    crawl_result = driver.execute_script(script)
   

    for i in range(0,len(crawl_result)):
        html = re.sub(r'\s+', ' ', crawl_result[i].get_attribute('outerHTML').strip())
        soup = bs(html,"lxml")
        for rs in soup.find_all('div'):
            test=[rs.text]
            if(np.amax(text_clf.predict_proba(test))>=0.8):
                print(rs.text,np.amax(text_clf.predict_proba(test)),text_clf.predict(test))
                print()
                print()
    
    driver.close()

detectDarkPattern("fsdf")